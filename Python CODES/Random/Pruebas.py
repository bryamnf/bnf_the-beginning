import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def Leer_Datos(archivo):
    """
    Leer datos del archivo salida del BS-Solctra
    """
    datos = pd.read_csv(archivo, delimiter='\t', names=['coord_x', 'coord_y', 'coord_z', 'mag_B'], skiprows=1, skipfooter=1, engine='python')
    # datos = np.loadtxt(archivo, delimiter = '\t', skiprows=1)
    # print('Datos leídos de "{}"'.format(archivo))
    # Usar pandas aquí más rápido y robusto pero es más complicado manejar los datos
    # Es posible que np.loadtxt() sea una opción viable en este caso
    return datos


def Cartesianas_a_Cilindricas_Puntos(vector, nRot):
    """
    Transformación de coord cartesianas a polares: (x, y, z) -> (r, phi ,z)
    array en cartesianas -> arry en cilindricas
    """
    vector_cil = np.array(vector)
    r = np.sqrt(vector[0]**2 + vector[1]**2)
    # cambiar a arccos o arcsen para evitar la division por cero
    phi = np.arctan(vector[1]/vector[0])

    if vector[0] > 0 and vector[1] > 0:
        phi = 2*np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el primer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    elif vector[0] < 0 and vector[1] > 0:
        phi = 2*np.pi + np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el segundo cuadrante {0:.2f}°'.format(phi*180/np.pi))
    elif vector[0] < 0 and vector[1] < 0:
        phi = 2*np.pi + np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el tercer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    else:
        phi = 2*np.pi + 2*np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el cuarto cuadrante {0:.2f}°'.format(phi*180/np.pi))

    vector_cil[0] = r
    vector_cil[1] = phi
    vector_cil[2] = vector[2]
    # print('vector en cilíndricas: {}'.format(vector))

    return vector_cil


def Calculo_iota(archivo):
    """
    Función que calcula la transformada rotacional en el ángulo 'phi_0', dados los datos en 'archivo'
    """
    datos = Leer_Datos(archivo)         # Se lee el archivo con los datos de la trayectoria
    nPasos, _ = datos.shape             # Se obtiene el número de puntos de trayectoria calculados
    nRot = 0                            # número de rotaciones
    eje_magnetico_r = 0.2477
    eje_magnetico_z = 0.0
    x = np.arange(nPasos)
    iota_aprox = np.zeros(nPasos)
    for iPaso in range(1, nPasos):  # se comienza a iterar desde el segundo punto
        # Se toman dos puntos para calcular el vector campo magnético
        # print('Iteración {}:'.format(iPaso))
        puntoA = np.array([datos['coord_x'][iPaso - 1], datos['coord_y'][iPaso - 1], datos['coord_z'][iPaso - 1]])
        puntoB = np.array([datos['coord_x'][iPaso], datos['coord_y'][iPaso], datos['coord_z'][iPaso]])

        # Se obtienen las conmponentes de los puntos en coord cilíndricas
        puntoA_cilindricas = np.zeros(3, dtype=np.float64)
        puntoB_cilindricas = np.zeros(3, dtype=np.float64)
        puntoA_cilindricas = Cartesianas_a_Cilindricas_Puntos(puntoA, nRot)
        puntoB_cilindricas = Cartesianas_a_Cilindricas_Puntos(puntoB, nRot)

        # Se evalúa la componente toroidal para determinar el número de rotaciones dadas
        if abs(puntoA_cilindricas[1] - puntoB_cilindricas[1]) > np.pi:
            nRot += 1
            puntoB_cilindricas = Cartesianas_a_Cilindricas_Puntos(puntoB, nRot)
        theta_sup = np.arctan((puntoA_cilindricas[2]-eje_magnetico_z)/(puntoA_cilindricas[0]-eje_magnetico_r))
        iota_aprox[iPaso - 1] = theta_sup/puntoA_cilindricas[1]
    print(iota_aprox[-1])

    plt.plot(x, iota_aprox)
    plt.xlabel("Iteración")
    plt.ylabel("Iota aproximado")
    plt.title("Cálculo de Iota")
    plt.show()


if __name__ == '__main__':
    archivo = sys.argv[1]
    Calculo_iota(archivo)
