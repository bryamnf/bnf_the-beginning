# Realizado por: Bryam Núñez Flores
# Última edicción funcional: Jueves 19 de marzo 2020
import numpy as np


def LeerDatos(archivo):
    """
    Leer datos del archivo salida del BS-Solctra
    """
    datos = np.loadtxt(archivo, delimiter="\t", skiprows=1)
    # Se tiene un array de la forma [d_x,d_y,d_z,B] donde d_i es la posición
    # en coordenadas cartesianas de la partícula y B es la magnitud del campo
    # magnético
    # el contador del array empieza en 0.
    return datos


def AnguloEnRadianes(x, y):
    """
    Dado a que  la función arctan solo tiene está defina para ángulos de
    [pi/2, -pi/2] se crea una función que de los angulos en radianes pero
    que esté definida desde ¡¡¡¡[0, 2pi[ !!!!.
    X y Y representan los catétos de un triangulo rectangulo en el primer
    cuadrante
    El output es el angulo en radianes.
    """
    if x*y > 0. and x > 0.:  # primer cuadrante
        angulo = np.arctan(y/x)
    elif x*y < 0. and x < 0.:  # segundo cuadrante
        angulo = np.arctan(y/x) + np.pi
    elif x*y > 0. and x < 0.:  # tercer cuadrante
        angulo = np.arctan(y/x) + np.pi
    elif x*y < 0. and x > 0.:  # cuarto cuadrante
        angulo = -np.arctan(-1*y/x) + 2*np.pi
    elif x == 0. and y == 1.:  # cuando se indefine arctan en pi/2
        angulo = np.pi/2
    elif x == 0. and y == -1.:  # cuando se indefine arctan en 3pi/2
        angulo = (3*np.pi)/2
    elif y == 0. and x == 1.:  # cuando se tiene angulo = 0
        angulo = 0.
    else:  # cuando se tiene angulo = pi radianes
        angulo = np.pi
    return angulo


def Cartesianas_a_Cilindricas_Puntos(vector_Datos):
    """
    Transformación de coord cartesianas a polares: (x, y, z) -> (r, phi ,z)
    array en cartesianas -> arry en cilindricas
    """
    vector_cil = np.zeros(3)
    r = np.sqrt(vector_Datos[0]**2 + vector_Datos[1]**2)
    # cambiar a arccos o arcsen para evitar la division por cero
    phi = AnguloEnRadianes(vector_Datos[0], vector_Datos[1])
    vector_cil[0] = r
    vector_cil[1] = phi
    vector_cil[2] = vector_Datos[2]
    # print('vector en cilíndricas: {}'.format(vector))
    return vector_cil


def Rotational_Angle(vector_cil, eje_magnetico_r=0.2477, eje_magnetico_z=0.0):
    numerador = vector_cil[2] - eje_magnetico_z
    denominador = vector_cil[0] - eje_magnetico_r
    angulo = AnguloEnRadianes(denominador, numerador)
    return angulo


archivo = '/home/bnf/Escritorio/Repositorios/plasmatec/Datos/results_49865.meta.cnca/path048.txt'
p = LeerDatos(archivo)
j = Cartesianas_a_Cilindricas_Puntos(p[0])
print(Rotational_Angle(j))

# y la siguiente sección lo que hace es leer el archivo
"""datos = np.loadtxt('/home/bnf/Escritorio/Repositorios/path048.txt', skiprows=1)
i = 0
while i < len(datos):
    CoordenadasMagneticas(datos[i][0], datos[i][1], datos[i][2])
    i = i + 1"""
