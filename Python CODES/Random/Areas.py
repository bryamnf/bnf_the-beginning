import numpy as np
from matplotlib import pyplot as plt
from skimage.measure import EllipseModel
from matplotlib.patches import Ellipse


def LeerDatos(archivo):
    datos = np.loadtxt(archivo, usecols=(0, 1), skiprows=0)
    return datos


def GraficasDeContorno(archivo, lim_inferior, lim_superior):
    datos = LeerDatos(archivo)
    n, _ = datos.shape
    r = datos[:, 0]
    z = datos[:, 1]
    delta = lim_superior-lim_inferior
    x = np.zeros(delta)
    y = np.zeros(delta)
    points = np.zeros((delta, 2))
    for j in range(lim_inferior, lim_superior):
        x[j-lim_inferior] = datos[j][0]
        y[j-lim_inferior] = datos[j][1]
        points[j-lim_inferior] = datos[j]
    return points, x, y, r, z


def ellipse(points, x, y, r, z):
    ell = EllipseModel()
    ell.estimate(points)

    xc, yc, a, b, theta = ell.params
    area = np.pi*a*b

    print("center = ",  (xc, yc))
    print("angle of rotation = ",  theta)
    print("axes = ", (a, b))
    print("Área = ", area)

    fig, axs = plt.subplots(2, 1, sharex=False, sharey=False)
    axs[0].plot(r, z)
    axs[0].plot(x, y)

    axs[1].plot(x, y)
    axs[1].scatter(xc, yc, color='black', s=100)
    ell_patch = Ellipse((xc, yc), 2*a, 2*b, theta*180/np.pi, edgecolor='black', facecolor='none')

    axs[1].add_patch(ell_patch)
    plt.show()
    print(points)


archivo = '/home/bnf/Escritorio/Repositorios/plasmatec/Cálculo de errores/prueba0.txt'
lim_inferior = 9895
lim_superior = 9995
points, x, y, r, z = GraficasDeContorno(archivo, lim_inferior, lim_superior)
ellipse(points, x, y, r, z)
