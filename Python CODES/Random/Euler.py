# Creado por Bryam Núñez Flores
# Última fecha de edición 10-10-19
"""
El programa implementa el método de Euler para la aproximación de ecuaciones
diferenciales del tipo f'=(x, y), y(0)=x0
IMPORTANTE en la linea 37 es donde se define la función a aproximar
"""
# Se importa numpy para cálculos básicos
# Se importa pyplot para generar una gráfica
import numpy as np
from matplotlib import pyplot as plt

"""
x0 y y0 son las condicones iniciales, xf nos dice la ubicación del yf que
deseamos calcular y N es el número de intervalos (equi-espaciados) que deseamos
"""


def MetodoNumericoEuler(x0, y0, xf, N):
    # h es ancho del intervalo
    h = (xf-x0)/N
    # np.zeros genera un array de ceros del tamaño de intervalos
    x = np.zeros(N)
    y = np.zeros(N)
    # se define la condicón inicial en x
    x[0] = x0
    # se generan todos los x equi-espaciados, se Empieza en 1 xq ya se
    # estableción la condición inicial en x
    for i in range(1, N):
        x[i] = x[i-1] + h
    # se define la condicón inicial en y
    y[0] = y0
    for i in range(1, N):
        # se generan todas las aproximaciones, se empieza en 1 xq ya se
        # estableció la condición inicial en y
        # IMPORTANTE en la linea 37 es donde se define la función a aproximar
        y[i] = y[i-1] + h*(-1*(x[i-1])**2 + y[i-1] + 1)
        # solo se debe modificar lo que está después de la h
    for i in range(N):
        print(x[i], y[i])
    print("Valor de h=", h)
    # Proceso de generación de la gráfica
    plt.plot(x, y)
    plt.xlabel("Valores de x")
    plt.ylabel("Valores de y")
    plt.title("Método de euler")
    plt.show()


MetodoNumericoEuler(0, 0.5, 2, 1000)
