# Integracion por metodo NC trapezoide

# Limites de integracion a y b de interes
a = 0.0
b = 3.0

# Numero de intervalos para calculo numerico
n = 8

# Aqui se define la funcion que se va a integrar.
# Los valores de x y F(x) en cada iteracion se presentan en pantalla para
# llevar un rastro de la ejecucion del calculo.
def F(x):
    print(" x   f(x)    : ", x, x*x)
    return x*x

# Aqui se establecen los pesos wi para cada termino de la sumatoria.
# Deben corresponder con la regla integral en aplicacion.
def PesoWi(i,h):
    if ((i==1) or (i==n)): wi = h/2.0
    else: wi = h
    return wi

# Se determina el ancho del intervalo de integracion
h = (b-a)/(n-1)

# Se inicializa el valor de la sumatoria
sumatoria = 0.0

# Se realiza la sumatoria que corresponde a la integracion numerica
for i in range (1, n + 1):
    valorxi = a + (i - 1)*h # Valor de x para el termino i
    pesowitermino = PesoWi(i, h) # Valor del peso w para el termino i
    sumatoria = sumatoria + pesowitermino * F(valorxi)

print("Integral NC trapezoide = ", sumatoria)
print()

# Se realiza la misma integral utilizando la biblioteca SciPy
import scipy.integrate as integralscipy

# Calculo de la integral con la funcion general de integracion de SciPy
# res, err = integralscipy.quad(F,a,b)

# Calculo de integracion Gaussiana de SciPy
res, err = integralscipy.quadrature(F,a,b)

print("Integral SciPy = ", res)
