# Derivacion por diferencias hacia adelante, atras y centrada

# Valor de x donde se desea calcular la derivada de la funcion F(x)
x = 8.0

# Aqui se define la funcion que se va a integrar.
def F(x):
    return x*x

# Valor de parametro h para el calculo de la derivada
h = 0.0001

# Se realiza el calculo numerico de la derivada por cada metodo
print("Derivada hacia adelante = ", (F(x + h) - F(x))/h)
print("Derivada hacia atras = ", (F(x) - F(x - h))/h)
print("Derivada centrada = ", (F(x + h) - F(x - h))/(2*h))
