import numpy as np
import matplotlib.pyplot as plt

def Derivada(funcion,valor_evaluacion,metodo='centrada',h=0.5):
    '''Calcular la derivada de una funcion en un determinado valor
    Parametros
    ----------
    funcion : funcion a derivar
    valor_evaluacion : valor en el que se calculara la derivada de la funcion
    metodo : metodo de calculo de la derivada entre 'centrada', 'adelante', 'atras'
    h : valor del ancho del intervalo de calculo de la derivada
    '''
    if metodo == 'centrada':
        return (funcion(valor_evaluacion + h) - funcion(valor_evaluacion - h))/(2*h)
    elif metodo == 'adelante':
        return (funcion(valor_evaluacion + h) - funcion(valor_evaluacion))/h
    elif metodo == 'atras':
        return (funcion(valor_evaluacion) - funcion(valor_evaluacion - h))/h
    else:
        raise ValueError("Debe escoger entre 'centrada', 'adelante', 'atras'.")

# Valores sobre los que se calculara la derivada
x = np.linspace(0,5*np.pi,100)

# Calculos de las derivadas numericas por cada metodo para la funcion de interes
dydxcentrada = Derivada(np.sin,x)
dydxadelante = Derivada(np.sin,x,'adelante')
dydxatras = Derivada(np.sin,x,'atras')

# Evaluacion de la derivada analitica de la funcion de interes
dydxanalitica = np.cos(x)

# Generacion de un grafico para comparar los resultados
fig = plt.figure()
plt.plot(x,dydxcentrada,'r.',label='Diferencias centradas')
plt.plot(x,dydxadelante,'y.',label='Diferencias adelante')
plt.plot(x,dydxatras,'g.',label='Diferencias atras')
plt.plot(x,dydxanalitica,'b',label='Valor exacto')
plt.title('Calculo de la derivada de una funcion por diferentes metodos')
plt.legend(loc='upper right')
fig.savefig("graph.png")
